#! python3.
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 15 16:19:20 2017

@author: dacrands
"""
import openpyxl
from datetime import date

wb = openpyxl.load_workbook('coding-hours.xlsx')
sheet = wb.get_sheet_by_name('Program Time')

print(""" What langague where your using?:
        -Python ---> 1
        -R --------> 2
        -Wed Dev --> 3
        -Java -----> 4""")

# Gets user input and uses it as the column value
lang = int(input('Enter number: '))
index = 1

for rowNum in range(2, sheet.max_row):
    if sheet.cell(row=rowNum, column=lang+1).value is None:
        index = sheet.cell(row=rowNum, column=1).value
        break

time = int(input('Please enter your coding time in minutes >>  '))
sheet.cell(row=index+1, column=lang+1).value = time
sheet.cell(row=index+1, column=lang+5).value = str(date.today())


