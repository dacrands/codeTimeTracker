#! python3.
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 15 16:19:20 2017

@author: dacrands
"""
import openpyxl
from datetime import date

wb = openpyxl.load_workbook('coding-hours.xlsx')
sheet = wb.get_sheet_by_name('Program Time')

# Gets user input and uses it as the column value
index = 1
langList = [1,2,3,4]  #use item in self.dict

while True:
    try:
        print(""" What langague where your using?:
        -Python ---> 1
        -R --------> 2
        -Wed Dev --> 3
        -Java -----> 4""")

        langChoice = int(input('Enter number: '))
        lang = langList.index(langChoice) + 1
        break
    
    except ValueError:
        print("Please make sure your input matches the prompt!")

while True:
    try:
        codeTime = int(input('Please enter your coding time in minutes >>  '))
        break
    
    except ValueError:
        print("Please make sure you're entering a number!")
        
    

for rowNum in range(2, sheet.max_row):
    if sheet.cell(row=rowNum, column=lang+1).value is None:
        index = sheet.cell(row=rowNum, column=1).value
        break



sheet.cell(row=index+1, column=lang+1).value = codeTime
sheet.cell(row=index+1, column=lang+5).value = str(date.today())

while True:
    try:
        wb.save('coding-hours.xlsx')
        break
    except PermissionError:
        input("Make sure file is closed. Close file then press <enter>")
        
